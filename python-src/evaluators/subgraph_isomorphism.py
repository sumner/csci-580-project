import itertools as it
from typing import Generator, Tuple

from evaluators.base import BaseEvaluator
from graph import Graph
from util import INF


class Evaluator(BaseEvaluator):
    def __init__(self, pattern_graph_filename):
        self.pattern_graph = Graph(
            pattern_graph_filename,
            Graph.Formats.ADJACENCY_LIST,
        )

    def seed_queue(self, edge_list):
        for e_list in it.combinations(edge_list, self.pattern_graph.n_edges):
            g = Graph.from_edges(e_list)
            yield (INF, 0, g)

    def isomorphic(self, H: Graph) -> bool:
        if self.pattern_graph.n != H.n or self.pattern_graph.n_edges != H.n_edges:
            return False

        gen_pattern = lambda adj_list: ('|'.join(sorted(["{},{}".format(st,
            end[0]) for st, edges in adj_list.items() for end in edges])))
        graph_pattern = gen_pattern(self.pattern_graph.adjacency_list)
        perm_pattern = gen_pattern(H.adjacency_list)
        for matching in it.permutations(self.pattern_graph.nodes):
            renames = {k: v for k, v in zip(H.nodes, matching)}

            edges = []
            for edge in perm_pattern.split('|'):
                a, b = edge.split(',')
                a = renames[a] if a in renames else a
                b = renames[b] if b in renames else b
                edges.append("{},{}".format(a, b))
                #edges.append("{},{}".format(b, a))
            reord_pattern = '|'.join(sorted(edges))

            if reord_pattern == graph_pattern:
                return True
        return False

    def evaluate(
            self,
            G: Graph,
            H: Graph,
    ) -> Generator[Tuple[float, float, Graph], None, None]:
        for node in H.nodes:
            for incident_edge in G.adjacency_list[node]:
                if incident_edge not in H.adjacency_list[node]:
                    yield (0, 0, H.expand(node, incident_edge))

    def reevaluate(self, p_e: float, p_o: float, H: Graph) -> bool:
        if H.n > self.pattern_graph.n or H.n_edges >= self.pattern_graph.n_edges:
            return False
        return True

    def output(self, p_e: float, p_o: float, H: Graph) -> bool:
        return self.isomorphic(H)
