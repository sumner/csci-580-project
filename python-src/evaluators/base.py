from typing import Generator, Tuple

from graph import Graph


class BaseEvaluator:
    def evaluate(
            self,
            G: Graph,
            H: Graph,
    ) -> Generator[Tuple[float, float, Graph], None, None]:
        raise NotImplementedError(
            'Implementors of EvaluatorBase must implement evaluate')

    def reevaluate(self, p_e: float, p_o: float, H: Graph) -> bool:
        raise NotImplementedError(
            'Implementors of EvaluatorBase must implement reevaluate')

    def output(self, p_e: float, p_o: float, H: Graph) -> bool:
        raise NotImplementedError(
            'Implementors of EvaluatorBase must implement output')
