from collections import defaultdict
from typing import Any, Tuple, Set, Dict
from copy import deepcopy


class Graph:
    nodes: Set = set()
    adjacency_list: Dict[str, Set[Tuple[Any, float]]] = {}
    edge_list: Tuple[Any, float, Any] = []
    num_edges = 0

    def __init__(
            self,
            graph_filename=None,
            graph_format=None,
            nodes=None,
            adjacency_list=None,
            edge_list=None
    ):
        if graph_filename:
            with open(graph_filename) as gf:
                self.nodes, self.adjacency_list, self.edge_list = Graph.parse(
                    gf, graph_format)
                self.perimeter_len = None
        elif nodes:
            self.nodes = nodes
            self.adjacency_list = adjacency_list
            self.edge_list = edge_list
            self.perimeter_len = None

    def parse(graph_file, graph_format):
        return {
            Graph.Formats.ADJACENCY_LIST: Graph.Formats.parse_adjacency_format
        }[graph_format](graph_file)

    def __lt__(self, other):
        return self.n_edges < other.n_edges

    def __eq__(self, other):
        clean = lambda x: {k: v for k, v in x.items() if v}
        return clean(self.adjacency_list) == clean(other.adjacency_list)

    def __str__(self):
        return f'Graph\n  ' + '\n  '.join([
            f'{k}: {[e for e in v]}' for k, v in self.adjacency_list.items()
        ])

    @staticmethod
    def from_node(node):
        return Graph(nodes=set([node]), adjacency_list=defaultdict(set))

    @staticmethod
    def from_edges(edges):
        adjacency_list=defaultdict(set)
        edge_list = []
        for a, edge_weight, b in edges:
            edge_weight = float(edge_weight)

            adjacency_list[b].add((a, edge_weight))
            adjacency_list[a].add((b, edge_weight))
        return Graph(nodes=adjacency_list.keys(), adjacency_list=adjacency_list)

    @property
    def n(self):
        return len(self.nodes)

    @property
    def n_edges(self):
        return sum(len(edges) for _, edges in self.adjacency_list.items()) // 2

    @property
    def perimeter(self):
        if not self.perimeter_len:
            self.perimeter_len = 0
            for _, edges in self.adjacency_list.item():
                self.perimeter_len += sum(e_len for _, e_len in edges)

        return self.perimeter_len

    def expand(self, node: Any, edge: Tuple[Any, int]) -> 'Graph':
        new_nodes = set([*self.nodes, edge[0]])
        new_adjacency_list = deepcopy(self.adjacency_list)

        new_adjacency_list[edge[0]].add((node, edge[1]))
        new_adjacency_list[node].add((edge[0], edge[1]))

        return Graph(nodes=new_nodes, adjacency_list=new_adjacency_list)

    class Formats:
        ADJACENCY_LIST = 0

        def parse_adjacency_format(graph_file):
            adjacency_list = defaultdict(set)
            edge_list = []
            for line in graph_file:
                a, edge_weight, b = line.split()
                edge_weight = float(edge_weight)

                adjacency_list[b].add((a, edge_weight))
                adjacency_list[a].add((b, edge_weight))
                edge_list.append((a, edge_weight, b))

            nodes = set(adjacency_list.keys())
            return nodes, adjacency_list, edge_list
