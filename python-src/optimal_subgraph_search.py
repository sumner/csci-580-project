#! /usr/bin/env python
"""
Optimal Subgraph Search Program
"""

import argparse
import importlib.util
from queue import PriorityQueue
from typing import List, cast

from evaluators import BaseEvaluator
from graph import Graph
from itertools import combinations

# Parse arguments =============================================================
parser = argparse.ArgumentParser(description='Optimal Subgraph Search Program')
parser.add_argument('input_graph', help='specify a file with the input graph')
parser.add_argument(
    '-f',
    '--format',
    default=Graph.Formats.ADJACENCY_LIST,
    help='specify the format of the input graph')
parser.add_argument(
    'evaluator',
    help='file containing the evaluator function for a given subgraph')

args, unknownargs = parser.parse_known_args()

# Read the graph
G = Graph(args.input_graph, args.format)

# Read in the evaluator
spec = importlib.util.spec_from_file_location("evaluator", args.evaluator)
evaluator = importlib.util.module_from_spec(spec)
if evaluator is None:
    raise ImportError('Invalid Module')
spec.loader.exec_module(evaluator)

evaluator = evaluator.Evaluator(*unknownargs)
evaluator = cast(BaseEvaluator, evaluator)

# The Evaluation Queue contains elements of the form (p_e, p_o, H)
# where p_e is the evaluation priority of the subgraph H, and p_o is the output
# priority of the subgraph H.
evaluation_queue: PriorityQueue = PriorityQueue(100)
output_queue: PriorityQueue = PriorityQueue(100)

# Seed the evaluation queue with all nodes in G
for graph in evaluator.seed_queue(G.edge_list):
    evaluation_queue.put(graph)

while not evaluation_queue.empty():
    potential_subgraph  = evaluation_queue.get()

    if evaluator.output(*potential_subgraph):
        output_queue.put(potential_subgraph[1:])

    _, _, H = potential_subgraph
    for new_subgraph in evaluator.evaluate(G, H):
        (p_e, p_o, H) = new_subgraph
        if evaluator.reevaluate(*potential_subgraph):
            evaluation_queue.put(potential_subgraph)

isomorphic_subgraphs: List[Graph] = []
print(len(output_queue.queue))
while not output_queue.empty():
    p_o, graph = output_queue.get()
    if graph not in isomorphic_subgraphs:
        isomorphic_subgraphs.append(graph)
print("Geometrically isomorphic subgraphs:")

print(len(isomorphic_subgraphs))
for graph in isomorphic_subgraphs:
    print(str(graph))
