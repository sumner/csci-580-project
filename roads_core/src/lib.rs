use serde_derive::{Serialize, Deserialize};

#[derive(Copy, Clone, Debug, Serialize, Deserialize)]
pub struct Region {
    pub lat0: f32,
    pub lat1: f32,
    pub lon0: f32,
    pub lon1: f32,
}

impl Region {
    pub fn empty() -> Region {
        use std::f32::{INFINITY, NEG_INFINITY};

        Region {
            lat0: INFINITY,
            lat1: NEG_INFINITY,
            lon0: INFINITY,
            lon1: NEG_INFINITY,
        }
    }

    pub fn correct_aspect(mut self) -> Region {
        let dlat = self.lat1 - self.lat0;
        let dlon = self.lon1 - self.lon0;
        let size = dlat.max(dlon);
        self.lat0 -= (size - dlat) / 2.;
        self.lat1 += (size - dlat) / 2.;
        self.lon0 -= (size - dlon) / 2.;
        self.lon1 += (size - dlon) / 2.;
        self
    }

    pub fn flip_lat(mut self) -> Region {
        use std::mem::swap;
        swap(&mut self.lat0, &mut self.lat1);
        self
    }

    pub fn flip_lon(mut self) -> Region {
        use std::mem::swap;
        swap(&mut self.lon0, &mut self.lon1);
        self
    }

    pub fn append(self, pos: Position) -> Region {
        Region {
            lat0: self.lat0.min(pos.lat),
            lat1: self.lat1.max(pos.lat),
            lon0: self.lon0.min(pos.lon),
            lon1: self.lon1.max(pos.lon),
        }
    }
}

#[derive(Copy, Clone, Debug, Serialize, Deserialize)]
pub struct Position {
    pub lat: f32,
    pub lon: f32,
}

impl Position {
    // Calculate distance (meters) using the Haversine formula.
    pub fn distance(self, other: Position) -> f32 {
        static RADIUS: f32 = 6371e3;

        fn deg2rad(d: f32) -> f32 { (d / 180.) * std::f32::consts::PI }

        let phi1 = deg2rad(self.lat);
        let phi2 = deg2rad(other.lat);
        let dphi = (phi2 - phi1) / 2.;
        let dlam = deg2rad(other.lon - self.lon) / 2.;

        let mut sin_dphi_sq = dphi.sin();
        sin_dphi_sq *= sin_dphi_sq;

        let mut sin_dlam_sq = dlam.sin();
        sin_dlam_sq *= sin_dlam_sq;

        let a = sin_dphi_sq + phi1.cos() * phi2.cos() * sin_dlam_sq;
        RADIUS * 2. * a.sqrt().asin()
    }

    pub fn norm(self, scale: f32, reg: &Region) -> Self {
        Position {
            lat: scale * (self.lat - reg.lat0) / (reg.lat1 - reg.lat0),
            lon: scale * (self.lon - reg.lon0) / (reg.lon1 - reg.lon0),
        }
    }

    pub fn tuple(self) -> (f32, f32) { (self.lon, self.lat) }

    pub fn offset_tuple(self, x: f32, y: f32) -> (f32, f32) {
        (self.lon + x, self.lat + y)
    }
}
