Want to do something that has:

- GPU programming
- Graph problem
- Metaprogramming?
- Approximate computing?

Ideas:

- Accelerate/optimize existing algorithm/application using GPU

  - MST
  - TSP
  - Rubik's cube
  - Sidequest
  - GPU accelerated statistical graph database

Papers that may be useful:

- https://www.cs.virginia.edu/~skadron/Papers/sc11_dymaxion_dist.pdf
- http://delivery.acm.org/10.1145/2610000/2600227/p239-khorasani.pdf?ip=138.67.129.18&id=2600227&acc=AUTHOR%2DIZED&key=4D4702B0C3E38B35%2E4D4702B0C3E38B35%2E4D4702B0C3E38B35%2E2C4C8AA27BDB1756&__acm__=1538856148_213eb37b39ee44472716089c78735d48

- Algorithm optimization
- Halo area on the map problem

Social Network Data

https://snap.stanford.edu/data/
