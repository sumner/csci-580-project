use osmpbfreader::objects::{Way as OsmWay, OsmObj};
use osmpbfreader::OsmPbfReader;
use serde_derive::{Serialize, Deserialize};
use failure::{Error, Fail};
use fnv::FnvHashMap;
use svg::node::{Node as SvgNode, element::{SVG, Polyline}};
use std::io::{Read, Seek};
use std::fmt;
use crate::{stage, prog};

pub use roads_core::{Position, Region};

#[derive(Copy, Clone, Debug, Serialize, Deserialize)]
pub struct Node {
    pub users: u16,
    pub osmid: u64,
    #[serde(flatten)]
    pub pos: Option<Position>,
}

#[derive(Debug, Copy, Clone)]
pub struct UnknownNode;

impl fmt::Display for UnknownNode {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "node data is missing")
    }
}

impl Fail for UnknownNode {}

impl Node {
    pub fn try_pos(&self) -> Result<Position, UnknownNode> {
        self.pos.ok_or(UnknownNode)
    }
}

fn append_node(reg: Region, node: &Node) -> Result<Region, UnknownNode> {
    Ok(reg.append(node.try_pos()?))
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Road {
    pub scale: u8,
    pub oneway: bool,
    pub roundabout: bool,
    pub nodes: Vec<u64>,
}

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
enum RoadDirection {
    Both,
    Forward,
    Backward,
}
use self::RoadDirection::*;

impl Road {
    pub fn try_from_osm<E>(obj: Result<OsmObj, E>) -> Option<Result<Road, E>> {
        match obj {
            Ok(obj) => Road::from_osm(obj).map(Ok),
            Err(e) => Some(Err(e)),
        }
    }

    pub fn from_osm(obj: OsmObj) -> Option<Road> {
        match obj {
            OsmObj::Way(way) => Road::from_osm_way(way),
            _ => None
        }
    }

    pub fn from_osm_way(way: OsmWay) -> Option<Road> {
        let hw = way.tags.get("highway")?.as_str();
        let mut direction = Both;

        let scale = match hw {
            "motorway" => { direction = Forward; 8 },
            "motorway_link" => 8,
            "trunk" | "trunk_link" => 7,
            "primary" | "primary_link" => 6,
            "secondary" | "secondary_link" => 5,
            "tertiary" | "tertiary_link" => 4,
            "unclassified" => 3,
            "residential" => 2,
            "service" | "track" => 1,
            _ => return None,
        };

        let roundabout = way.tags.get("junction").map(String::as_str) == Some("roundabout");
        if roundabout {
            direction = Forward;
        }

        direction = match way.tags.get("oneway").map(String::as_str) {
            None => direction,
            Some("yes") | Some("1") => Forward,
            Some("no") => Both,
            Some("-1") => Backward,
            Some(_) => return None,
        };

        let nodes = way.nodes.into_iter().map(|id| id.0 as u64);
        let nodes = match direction {
            Backward => nodes.rev().collect(),
            _ => nodes.collect(),
        };

        prog!(delta: 1);
        Some(Road {
            scale,
            oneway: direction != Both,
            roundabout,
            nodes,
        })
    }
}

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct Roads {
    pub roads: Vec<Road>,
    pub nodes: Vec<Node>,
}

impl Roads {
    pub fn new() -> Self {
        Default::default()
    }

    pub fn load(&mut self, mut reader: OsmPbfReader<impl Read + Seek + Send>) -> Result<(), Error> {
        // first pass
        stage!("Loading Ways");
        let mut ways = reader.iter().filter_map(Road::try_from_osm).collect::<Result<Vec<_>, _>>()?;

        // condense indices
        stage!(len: ways.len(), "Condensing IDs");
        let mut lookup: FnvHashMap<u64, u64> = Default::default();
        for way in &mut ways {
            prog!(delta: 1);
            for id in &mut way.nodes {
                let nid = *lookup.entry(*id).or_insert_with(|| {
                    self.nodes.push(Node { users: 0, osmid: *id, pos: None });
                    self.nodes.len() as u64 - 1
                });
                self.nodes[nid as usize].users += 1;
                *id = nid;
            }
        }

        // store roads
        if self.roads.is_empty() { self.roads = ways }
        else { self.roads.append(&mut ways) }

        // second pass
        stage!(len: lookup.len(), "Loading Nodes");
        reader.rewind()?;
        for obj in reader.iter() {
            let node = match obj? {
                OsmObj::Node(node) => node,
                _ => continue,
            };
            let id = match lookup.get(&(node.id.0 as u64)) {
                Some(&id) => id,
                None => continue,
            };
            let pos = Position {
                lat: node.decimicro_lat as f32 * 1.0e-7,
                lon: node.decimicro_lon as f32 * 1.0e-7,
            };

            self.nodes[id as usize].pos = Some(pos);

            prog!(delta: 1);
        }

        Ok(())
    }

    pub fn step_reduce(&mut self) -> usize {
        use std::collections::BTreeSet;

        // First pass, find possible merges.
        let mut merge_on = FnvHashMap::default();
        stage!(len: self.roads.len(), "Finding merge points");
        for (index, road) in self.roads.iter().enumerate() {
            prog!(delta: 1);

            let last = match road.nodes.last() {
                Some(&last) => last,
                None => continue,
            };

            // The last node is shared with only one other road.
            // We might be able to merge the two roads.
            if self.nodes[last as usize].users == 2 {
                merge_on.insert(last, index);
            }
        }

        // Second pass, try merges.
        stage!(len: self.roads.len(), "Merging roads");
        let mut remove = BTreeSet::new();
        let mut add = Vec::new();
        let mut missed = 0;
        for (index, road) in self.roads.iter().enumerate() {
            prog!(delta: 1);

            if remove.contains(&index) {
                // road segment will be removed, try again later
                missed += 1;
                continue
            }

            let middle = match road.nodes.first() {
                Some(&middle) => middle,
                None => continue,
            };

            // look for the road that wants to merge
            let with = match merge_on.get(&middle) {
                Some(&with) => with,
                None => continue,
            };

            // can't merge with self
            if with == index { continue }

            let other = &self.roads[with];

            // roads must be same type
            if other.oneway != road.oneway { continue }
            if other.scale != road.scale { continue }
            if other.roundabout != road.roundabout { continue }

            // remove merge point
            merge_on.remove(&middle);

            // TODO: merge 3 roads (probably not handled)

            // stage changes
            self.nodes[middle as usize].users -= 1; // will no longer be shared
            remove.insert(index);
            remove.insert(with);
            add.push(Road {
                scale: road.scale,
                oneway: road.oneway,
                roundabout: road.roundabout,
                nodes: other.nodes.iter().chain(road.nodes.iter().skip(1)).cloned().collect(),
            });
        }

        // remove merged roads
        for index in remove.into_iter().rev() {
            self.roads.swap_remove(index);
        }

        // add new roads
        self.roads.append(&mut add);

        missed
    }

    pub fn reduce(&mut self) {
        loop {
            // TODO: loop is slow
            if self.step_reduce() == 0 { break }
        }
    }

    pub fn to_svg(&self) -> Result<SVG, UnknownNode> {
        let reg = self.nodes.iter()
            .try_fold(Region::empty(), append_node)?
            .correct_aspect()
            .flip_lat();

        let mut svg = SVG::new();
        for road in &self.roads {
            let pts = road.nodes.iter()
                .map(|&i| Ok(self.nodes[i as usize].try_pos()?.norm(100., &reg).tuple()))
                .collect::<Result<Vec<_>, UnknownNode>>()?;
            svg = svg.add(stroke(Polyline::new()
                .set("points", pts)
                .set("fill", "none"),
                road.scale,
                road.roundabout,
                road.oneway));
        }

        Ok(svg)
    }
}

pub fn stroke<T: SvgNode>(mut svg: T, scale: u8, roundabout: bool, oneway: bool) -> T {
    let (r, g, b) = match (roundabout, oneway) {
        (false, false) => (0.75, 0.75, 0.75),
        (true, _) => (0.9, 0.2, 0.2),
        (_, true) => (0.2, 0.2, 0.9),
    };
    let mult = scale as f32 / 16.0 + 0.5;

    svg.assign("stroke", format!(
        "#{:02X}{:02X}{:02X}",
        (r * mult * 255.) as u8,
        (g * mult * 255.) as u8,
        (b * mult * 255.) as u8,
    ));
    svg.assign("stroke-width", 0.05 * mult);
    svg
}
