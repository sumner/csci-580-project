use lazy_static::lazy_static;
use std::time::Instant;
use std::sync::Mutex;

lazy_static! {
    pub static ref LAST_STAGE: Mutex<Option<Instant>> = Mutex::new(None);
}

#[macro_export]
macro_rules! stage {
    (len: $len:expr, $($x:tt)*) => (stage!($($x)*));
    ($($x:tt)*) => ({
        let now = std::time::Instant::now();
        if let Some(t) = ::std::mem::replace(&mut *crate::progress::LAST_STAGE.lock().unwrap(), Some(now)) {
            let duration = now.duration_since(t);
            eprintln!("Took {:.2}s.", duration.as_secs() as f64 + duration.subsec_micros() as f64 * 1e-6);
        }
        eprintln!($($x)*);
    });
}

#[macro_export]
macro_rules! prog {
    (delta: $delta:expr) => ();
}
