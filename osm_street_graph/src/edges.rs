use serde_derive::Serialize;
use crate::roads::{Roads, UnknownNode};
use fnv::{FnvHashMap, FnvHashSet};
use svg::node::element::{SVG, Line};
use crate::{stage, prog};
use roads_core::Position;

#[derive(Copy, Clone, Debug, Serialize)]
pub struct Edge {
    pub to: usize,
    pub dist: f32,
    pub scale: u8,
    pub reverse: Option<usize>,
}

#[derive(Debug, Clone, Serialize)]
pub struct Node {
    pub osmid: u64,
    pub pos: Position,
    pub edges: Vec<Edge>,
    pub incoming: Vec<EdgeRef>,
}

impl Node {
    pub fn push_edge(&mut self, edge: Edge) -> usize {
        self.edges.push(edge);
        self.edges.len() - 1
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
pub enum End {
    Front,
    Back,
}

#[derive(Debug, Clone, Serialize)]
pub struct Roundabout {
    pub nodes: Vec<EdgeRef>,
}

impl Roundabout {
    pub fn append(&mut self, mut other: Roundabout, end: End) {
        use std::mem;
        if end == End::Front { mem::swap(&mut self.nodes, &mut other.nodes) }
        self.nodes.append(&mut other.nodes);
    }
}

#[derive(Debug, Copy, Clone, Serialize, PartialEq, Eq, Hash)]
pub struct EdgeRef {
    pub node: usize,
    pub edge: usize,
}

#[derive(Debug, Default, Clone, Serialize)]
pub struct Edges {
    pub nodes: Vec<Node>,
    pub roundabouts: Vec<Roundabout>,
    #[serde(skip)]
    pub roundabout_lookup: FnvHashMap<(usize, End), usize>,
}

impl Edges {
    pub fn new() -> Self {
        Default::default()
    }

    pub fn push_roundabout(&mut self, roundabout: Roundabout) {
        let (first, last) = match (roundabout.nodes.first(), roundabout.nodes.last()) {
            (Some(a), Some(b)) => (a, b),
            _ => return,
        };
        let first = first.node;
        let last = self.nodes[last.node].edges[last.edge].to;

        if let Some(index) = self.roundabout_lookup.remove(&(first, End::Back)) {
            self.roundabouts[index].append(roundabout, End::Back);
            return
        }

        if let Some(index) = self.roundabout_lookup.remove(&(last, End::Front)) {
            self.roundabouts[index].append(roundabout, End::Front);
            return
        }

        self.roundabouts.push(roundabout);
        let index = self.roundabouts.len() - 1;
        self.roundabout_lookup.insert((first, End::Front), index);
        self.roundabout_lookup.insert((last, End::Back), index);
    }

    pub fn append_roads(&mut self, roads: &Roads) -> Result<(), UnknownNode> {
        stage!(len: roads.roads.len(), "Building Edges");

        let starting_len = self.nodes.len();
        self.nodes = roads.nodes.iter().map(|node| Ok(Node {
            pos: node.try_pos()?,
            edges: Vec::new(),
            incoming: Vec::new(),
            osmid: node.osmid,
        })).collect::<Result<_, _>>()?;

        for road in &roads.roads {
            prog!(delta: 1);

            let scale = road.scale;

            let mut roundabout = if road.roundabout {
                Some(Roundabout { nodes: Vec::with_capacity(road.nodes.len() - 1) })
            } else {
                None
            };

            for win in road.nodes.windows(2) {
                let a = win[0] as usize + starting_len;
                let b = win[1] as usize + starting_len;
                let dist = self.nodes[a].pos.distance(self.nodes[b].pos);

                let forward_edge = self.nodes[a].push_edge(Edge { to: b, dist, scale, reverse: None });
                if road.oneway {
                    self.nodes[b].incoming.push(EdgeRef { node: a, edge: forward_edge })
                } else {
                    let back_edge = self.nodes[b].push_edge(Edge { to: a, dist, scale, reverse: Some(forward_edge) });
                    self.nodes[a].edges[forward_edge].reverse = Some(back_edge);
                }

                if let Some(ref mut roundabout) = roundabout {
                    roundabout.nodes.push(EdgeRef {
                        node: a,
                        edge: forward_edge,
                    })
                }
            }

            if let Some(roundabout) = roundabout {
                self.push_roundabout(roundabout);
            }
        }

        Ok(())
    }

    pub fn append_roads_reduced(&mut self, roads: &Roads) -> Result<(), UnknownNode> {
        #[derive(Default)]
        struct MappingManager {
            lookup: FnvHashMap<u64, usize>,
        }

        #[derive(Debug)]
        struct ReadyNode {
            pub osmid: u64,
            pub in_index: u64,
            pub pos: Position,
            pub users: u16,
        }

        impl ReadyNode {
            pub fn commit(self, mapping: &mut MappingManager, edges: &mut Edges) -> usize {
                *mapping.lookup.entry(self.in_index).or_insert_with(|| {
                    edges.nodes.push(Node { pos: self.pos, edges: Vec::new(), incoming: Vec::new(), osmid: self.osmid });
                    edges.nodes.len() - 1
                })
            }
        }

        let to_ready = |in_index: u64| {
            let input = &roads.nodes[in_index as usize];
            Ok(ReadyNode {
                in_index,
                pos: input.try_pos()?,
                users: input.users,
                osmid: input.osmid,
            })
        };

        let mapping = &mut MappingManager::default();

        stage!(len: roads.roads.len(), "Building Reduced Edges");

        for road in &roads.roads {
            prog!(delta: 1);

            let scale = road.scale;
            let mut roundabout = if road.roundabout {
                Some(Roundabout { nodes: Vec::with_capacity(road.nodes.len() - 1) })
            } else {
                None
            };

            let start_in_index = match road.nodes.first() {
                Some(&i) => i,
                None => continue,
            };

            let mut dist = 0.;
            let ready = to_ready(start_in_index)?;
            let mut a = ready.commit(mapping, self);

            let road_last = road.nodes.len() - 1;
            for (in_index, last) in road.nodes
                .iter()
                .enumerate()
                .skip(1)
                .map(|(a, b)| (*b, a == road_last))
            {
                let ready = to_ready(in_index)?;
                dist += self.nodes[a].pos.distance(ready.pos);
                if ready.users > 1 || last {
                    let b = ready.commit(mapping, self);
                    let forward_edge = self.nodes[a].push_edge(Edge { to: b, dist, scale, reverse: None });

                    if road.oneway {
                        self.nodes[b].incoming.push(EdgeRef { node: a, edge: forward_edge })
                    } else {
                        let back_edge = self.nodes[b].push_edge(Edge { to: a, dist, scale, reverse: Some(forward_edge) });
                        self.nodes[a].edges[forward_edge].reverse = Some(back_edge);
                    }

                    if let Some(ref mut roundabout) = roundabout {
                        roundabout.nodes.push(EdgeRef {
                            node: a,
                            edge: forward_edge,
                        })
                    }

                    a = b;
                    dist = 0.;
                }
            }

            if let Some(roundabout) = roundabout {
                self.push_roundabout(roundabout);
            }
        }

        Ok(())
    }

    pub fn to_svg(&self) -> Result<SVG, UnknownNode> {
        use crate::roads::{Region, stroke};

        let reg = self.nodes.iter()
            .map(|n| n.pos)
            .fold(Region::empty(), Region::append)
            .correct_aspect()
            .flip_lat();

        let rounds: FnvHashSet<_> = self.roundabouts.iter()
            .flat_map(|r| r.nodes.iter())
            .cloned()
            .collect();

        let mut svg = SVG::new();
        for (i, a) in self.nodes.iter().enumerate() {
            for (j, edge) in a.edges.iter().enumerate() {
                let b = &self.nodes[edge.to];

                let (x0, y0) = a.pos.norm(100., &reg).tuple();
                let (x1, y1) = b.pos.norm(100., &reg).tuple();

                svg = svg.add(stroke(Line::new()
                    .set("x1", x0)
                    .set("x2", x1)
                    .set("y1", y0)
                    .set("y2", y1),
                    edge.scale,
                    rounds.contains(&EdgeRef { node: i, edge: j }),
                    edge.reverse.is_none()));
            }
        }

        Ok(svg)
    }
}
