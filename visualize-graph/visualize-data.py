#! /usr/bin/env python3
"""
Creates a graphvis visualization of a graph.
"""

import argparse
from typing import Set, TextIO, Tuple

from graphviz import Graph


def to_graph(file: TextIO) -> Tuple[Set, Set]:
    nodes = set()
    edges = set()
    for line in file:
        start, end = line.split()
        nodes.add(start)
        nodes.add(end)
        edges.add((start, end))

    return (nodes, edges)


parser = argparse.ArgumentParser('Visualize Graph data using GraphVis')
parser.add_argument(
    '-i',
    '--input-file',
    type=argparse.FileType('r'),
    help='the graph input file')
parser.add_argument(
    '-o', '--output-file', help='file to output the graph visualization to')
parser.add_argument('-s', '--subgraph', type=argparse.FileType('r'))

args = parser.parse_args()

subgraph_nodes, subgraph_edges = to_graph(args.subgraph)
graph_nodes, graph_edges = to_graph(args.input_file)

g = Graph('G', filename=args.output_file, format='dot')
g.attr(rankdir='LR')

# Graph nodes
# red_nodes = []
for node in graph_nodes:
    node_color = 'firebrick1' if node in subgraph_nodes else 'black'
    g.attr('node', shape='circle', color=node_color)
    g.node(node)

for edge in graph_edges:
    start, end = edge
    if edge not in subgraph_edges:
        g.edge(start, end, color='black')

for edge in subgraph_edges:
    start, end = edge
    g.edge(start, end, color='firebrick1')

with open(args.output_file, 'w+') as outfile:
    outfile.write(g.source)

args.input_file.close()
