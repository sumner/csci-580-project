use serde_derive::{Serialize, Deserialize};
pub use roads_core::Position;

#[derive(Copy, Clone, Debug, Serialize, Deserialize)]
pub struct Edge {
    pub to: usize,
    pub dist: f32,
    pub scale: u8,
    pub reverse: Option<usize>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Node {
    pub osmid: u64,
    pub pos: Position,
    pub edges: Vec<Edge>,
    pub incoming: Vec<EdgeRef>,
}

#[derive(Debug, Copy, Clone, Hash, PartialEq, Eq, Serialize, Deserialize)]
pub struct EdgeRef {
    pub node: usize,
    pub edge: usize,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Roundabout {
    pub nodes: Vec<EdgeRef>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Edges {
    pub nodes: Vec<Node>,
    pub roundabouts: Vec<Roundabout>,
}

impl Edges {
    pub fn edge(&self, e: EdgeRef) -> &Edge {
        &self.nodes[e.node].edges[e.edge]
    }
}
