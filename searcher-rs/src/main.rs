pub mod data;
use crate::data::*;

use failure::{Error, format_err};
use std::collections::{BTreeSet, VecDeque};
use hashbrown::HashSet;
use svg::node::Node as SvgNode;
use svg::node::element::{SVG, Polyline};
use roads_core::Region;
use structopt::StructOpt;
use num_cpus::get as num_cpus;
use rand::prelude::*;
use std::sync::Arc;
use std::path::PathBuf;
use std::time::{Instant, Duration};
use std::fmt;

#[derive(Debug, StructOpt)]
#[structopt(name = "searcher", about = "Search road data for roundabouts.")]
struct Opt {
    #[structopt(parse(from_os_str))]
    input: PathBuf,
    #[structopt(short = "t", long = "threads", help="override approx thread count")]
    threads: Option<usize>,
    #[structopt(long = "json", help="input is json not bincode")]
    json: bool,
    #[structopt(short = "d", long = "diam", help="diameter limit of roundabouts", default_value="100.0")]
    diameter_limit: f32,
    #[structopt(short = "n", long = "num", help="number of samples", default_value="1000000")]
    sample_limit: usize,
}

pub fn inv_diameter(chord: f64, arc: f64) -> f64 {
    use std::f64::EPSILON;
    use std::f64::consts::PI;

    if (arc - chord).abs() < 10. * EPSILON {
        // circle is infinite
        return 0.
    }

    // starting estimate
    // approx circumference as arc+chord
    let mut x = PI / (arc + chord);

    // newton's method
    loop {
        let (s, c) = (arc * x).sin_cos();
        let f = s - chord * x;
        x -= f / (arc * c - chord);
        if f.abs() < (5. * EPSILON) { return x }
    }
}

#[derive(Clone, Debug)]
pub struct Working {
    pub nodes: BTreeSet<usize>,
    pub last: usize,
    pub first: usize,
    pub length: f32,
    pub effective_diameter: f32,
    pub diameter_pair: Option<(u64, u64)>,
    pub branches: usize,
    pub choices: Vec<usize>,
}

impl Working {
    pub fn new() -> Working {
        Working {
            nodes: BTreeSet::new(),
            first: 0,
            last: 0,
            length: 0.,
            effective_diameter: 0.,
            diameter_pair: None,
            branches: 0,
            choices: Vec::with_capacity(16),
        }
    }

    pub fn clear_from_edge(&mut self, eref: EdgeRef, edge: &Edge) {
        self.nodes.clear();
        self.nodes.insert(eref.node);
        self.nodes.insert(edge.to);
        self.first = eref.node;
        self.last = edge.to;
        self.length = edge.dist;
        self.effective_diameter = edge.dist;
        self.diameter_pair = None;
        self.branches = 0;
    }

    pub fn random_extension<'a>(&mut self, data: &'a Edges, rand: &mut impl Rng) -> bool {
        let from = &data.nodes[self.last];

        self.choices.clear();
        let nodes = &self.nodes;
        let first = self.first;
        self.choices.extend((0..from.edges.len())
            .filter(|&i| {
                let e = &from.edges[i];
                e.reverse.is_none() && !nodes.contains(&e.to) || first == e.to
            }));


        let edge = match self.choices.choose(rand) {
            Some(&i) => &from.edges[i],
            None => return false,
        };

        self.branches += self.choices.len();

        let last_node = &data.nodes[self.last];
        for &node in &self.nodes {
            let a = &data.nodes[node];
            let d = a.pos.distance(last_node.pos);
            if d > self.effective_diameter {
                self.diameter_pair = Some((last_node.osmid, a.osmid));
                self.effective_diameter = d;
            }
        }

        self.last = edge.to;
        self.nodes.insert(edge.to);
        self.length += edge.dist;

        true
    }

    pub fn looped(&self) -> bool {
        self.looped_to(self.last)
    }

    fn looped_to(&self, node: usize) -> bool {
        self.first == node
    }
}

pub struct Timer {
    time: Instant,
}

impl Timer {
    pub fn new() -> Timer {
        Timer { time: Instant::now(), }
    }

    pub fn lap(&mut self) -> Duration {
        let now = Instant::now();
        let duration = now.duration_since(self.time);
        self.time = now;
        duration
    }

    pub fn lap_s(&mut self) -> f64 {
        let d = self.lap();
        d.as_secs() as f64 + d.subsec_micros() as f64 * 1e-6
    }

    pub fn duration(&mut self, text: impl fmt::Display) {
        let duration = self.lap_s();
        eprintln!("{} | in {:.3}s", text, duration);
    }

    pub fn rate(&mut self, text: impl fmt::Display, count: usize) {
        let duration = self.lap_s();
        eprintln!("{} | {:.3} per s ({:.3}s)", text, count as f64 / duration, duration);
    }
}

pub struct SearchContext {
    graph: Arc<Edges>,
    work: Working,
    found: usize,
    diameter_limit: f32,
    total_depth: usize,
    total_branches: usize,
}

impl SearchContext {
    pub fn new(graph: Arc<Edges>, diameter_limit: f32) -> SearchContext {
        SearchContext {
            graph,
            work: Working::new(),
            found: 0,
            diameter_limit,
            total_depth: 0,
            total_branches: 0,
        }
    }

    pub fn push(&mut self, nid: usize, eid: usize) {
        self.work.clear_from_edge(
            EdgeRef { node: nid, edge: eid },
            &self.graph.nodes[nid].edges[eid],
        );
    }

    pub fn go(&mut self, rand: &mut impl Rng) {
        let found = loop {
            if self.work.looped() {
                break true;
            }

            if self.work.effective_diameter > self.diameter_limit {
                break false;
            }

            if !self.work.random_extension(&*self.graph, rand) {
                break false;
            }
        };

        if found {
            self.found += 1;
            self.total_branches += self.work.branches;
            self.total_depth += self.work.nodes.len() - 2;
        }
    }
}

fn main() -> Result<(), Error> {
    use std::env;
    use std::fs::File;
    use flate2::{read::GzDecoder, write::GzEncoder, Compression};
    use bincode::deserialize_from as read_bincode;
    use serde_json::from_reader as read_json;
    use std::thread::spawn;

    let mut timer = Timer::new();

    let opt = Opt::from_args();
    let threads = opt.threads.unwrap_or(num_cpus());
    let sampled = opt.sample_limit * threads;

    let decoder = GzDecoder::new(File::open(opt.input)?);
    let edges: Edges = match opt.json {
        true => read_json(decoder)?,
        false => read_bincode(decoder)?,
    };

    timer.duration("Input Complete");

    let inds: Vec<(usize, usize)> = edges.nodes.iter()
        .enumerate()
        .flat_map(|(i, n)| n.edges.iter()
            .enumerate()
            .filter_map(move |(j, e)| if e.reverse.is_none() {
                Some((i, j))
            } else {
                None
            }))
        .collect();

    timer.duration("Found Starting Points");

    let graph = Arc::new(edges);
    let inds = Arc::new(inds);
    let diameter_limit = opt.diameter_limit;
    let sample_limit = opt.sample_limit;
    let threads: Vec<_> = (0..threads).map(|_| {
        let graph = graph.clone();
        let inds = inds.clone();
        spawn(move || {
            let mut rng = thread_rng();
            let mut ctx = SearchContext::new(graph, diameter_limit);
            for _ in 0..sample_limit {
                let (nid, eid) = *inds.choose(&mut rng).unwrap();
                ctx.push(nid, eid);
                ctx.go(&mut rng);
            }
            ctx
        })
    }).collect();

    let (found, branches, depth): (usize, usize, usize) = threads.into_iter().map(|thread| thread.join().unwrap())
        .fold((0, 0, 0), |(a, b, c), w| (a + w.found, b + w.total_branches, c + w.total_depth));

    timer.rate("Dispatch and Search", sampled);

    let edges: usize = graph.nodes.iter().map(|n| n.edges.len()).sum();
    let avedepth = depth as f64 / found as f64;
    let avebranches = branches as f64 / found as f64 ;
    println!("Edges: {}\nSearched: {}\nIn Roundabout: {}\nAverage Depth: {:.3}\nAverage Branches: {:.3}", edges, sampled, found, avedepth, avebranches);

    Ok(())
}
