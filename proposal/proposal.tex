% Documentation for acmart:
%       http://www.sigplan.org/sites/default/files/acmart/current/acmart.pdf
\documentclass[sigplan]{acmart}

% Get rid of massive copyright
\setcopyright{none}

\usepackage{verbatim}
\usepackage{booktabs}   % For formal tables: http://ctan.org/pkg/booktabs
\usepackage{subcaption} % For complex figures with subfigures/subcaptions
                        % http://ctan.org/pkg/subcaption

\usepackage{mdframed}
\theoremstyle{definition}
\newmdtheoremenv{definition}{Definition}

% Math
\usepackage{amsmath}
\usepackage{amsthm}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\B}{\mathbb{B}}
\newcommand{\Gw}{\mathbb{G}_w}
\newcommand{\Gu}{\mathbb{G}_u}

% Author Listings
\author{Jonathan Sumner Evans}
\affiliation{Colorado School of Mines}
\email{jonathanevans@mines.edu}

\author{Daichi Jameson}
\affiliation{Colorado School of Mines}
\email{djameson@mines.edu}

\author{Sam Sartor}
\affiliation{Colorado School of Mines}
\email{ssartor@mines.edu}

% Keywords
\keywords{high performance computing, optimal subgraph search, subgraph
    isomorphisms, large graphs, big data, Python, Rust, CUDA}

% Citation settings
\setcitestyle{sort}

\title{Fast Optimal Subgraph Search Within Proportionally Large Graphs}
\subtitle{CSCI 580 Research Project Proposal}
\date{\today}

\begin{document}

\begin{abstract}
    \textbf{(JE, DJ)}

    The optimal subgraph search problem is the problem of finding the best
    subgraph in a large graph $L$ according to a cost metric $f_c$. In this
    paper we formally define the optimal subgraph search problem and propose a
    project to investigate and improve the speed of a solution to this problem
    by utilizing various optimization techniques and approximation algorithms.
    Our project plan is broken down into multiple progressive 2-week segments
    where we iteratively create more efficient solutions. Creating an effective
    optimal subgraph search implementation would provide benefits to several
    fields including data analytics, cheminformatics, and digital cartography.

\end{abstract}

\maketitle

\section{Introduction (JE, DJ, SS)}

For our project, we plan to design and implement a performant algorithm for
finding optimal subgraphs within large graphs. To define this problem formally,
we first define $\Gw$ and $\Gu$:
\vspace{1em}
\begin{definition}
    Let $\Gw$ be the set of all possible weighted graphs, and let $\Gu$ be the
    set of all possible unweighted graphs.
\end{definition}
\vspace{1em}
A formal definition of the optimal subgraph search problem follows:
\vspace{1em}
\begin{definition}[Optimal Subgraph Search]
    Given a cost function $f_c : \Gw \rightarrow \R$, and some large graph
    $L \in \Gw$, find the subgraph $S \subseteq L$ such that $f_c(S)$ is
    minimized.
\end{definition}
\vspace{1em}

For this project, we will be focusing on the specific problem of finding a
relatively small graph within a proportionally larger graph. One specific use of
optimal subgraph search is the inexact subgraph isomorphism problem which is
defined as follows:
\vspace{1em}
\begin{definition}[Inexact Subgraph Isomorphism Problem]
    Given a pattern graph $P \in \Gw$, find $S \subseteq L$ which is most
    similar to $P$ as defined by $f_s : \Gw \times \Gw \rightarrow \R$.
\end{definition}
\vspace{1em}

To find the most similar subgraph, we apply optimal subgraph search with cost
function $f_c(S) = -f_s(S, P)$. Note that as similarity between $S$ and $P$
increases, the cost of $S$ decreases. This is the application of optimal
subgraph search which we will focus on for our project.

The inexact subgraph isomorphism problem is a generalization of the subgraph
isomorphism problem which has been heavily studied. % TODO citations
The subgraph isomorphism problem is defined as follows:
\vspace{1em}
\begin{definition}[Subgraph Isomorphism Problem]
    Given two graphs $P$ and $L$, determine whether or not $P$ is a subgraph of
    $L$.
\end{definition}
\vspace{1em}

To solve the subgraph isomorphism problem, we convert all the unweighted edges
in $P$ and $L$ to edges with weight 1. Then we apply the inexact subgraph
isomorphism problem with
\begin{align*}
    f_s(A, B) =
    \begin{cases}
        1 & A = B \\
        0 & A \neq B
    \end{cases}.
\end{align*}

In the next subsection, we discuss the applications of the subgraph isomorphism
problem as well as the more generalized problems described above. We then
discuss the challenges we anticipate facing during this project. We describe the
tools and approach we intend to use before explaining our evaluation strategy.
Then we conclude by describing a tentative timeline for completing our project.

\subsection{Applications (JE, SS)}

The optimal subgraph search problem and the subgraph isomorphism problem are
computationally challenging but have countless useful applications in a number
of fields including, but not limited to, analysis of social networks, chemistry,
computer vision, and digital cartography.

Our original motivation for exploring this problem was an application related to
social networks. Consider a social network in the form of a graph where people
are represented as nodes and friendships are represented as edges. Nodes store
information about each person (i.e. interests, gender, school), and the edges
have weights representing the strength of the corresponding friendship. The task
is to find sets of people in the social network who have a given, shared
interest and who have mutually strong friendship bonds as compared to their
bonds to people outside of that set of friends.

For example, find four people who are interested in Computer Science and who
have strong mutual friendships, but have very few other mutual friends. This is
a generalization of the clique decision problem, where a graph is searched for
completely connected islands of a given size.

Another application of the subgraph problem is in chemistry, specifically the
field of cheminformatics. One of the primary problems is to determine whether or
not a molecule (or database of molecules) contains a certain substructure
\cite{pmid23815292}. For example, chemists can search PubChem, a widely used
database of chemical compounds, for chemicals which have given properties.
Often, this involves searching for particular combinations or structures of
atoms within each chemical \cite{doi:10.1093/nar/gkv951}. These applications use
currently subgraph isomorphisms, however using a generalized optimal subgraph
search can allow for more granular queries because of the arbitrary nature of
the cost function.

Digital cartography commonly involves searching for geographic structures within
enormous maps of cities, states, and countries. Such searches are necessary for
classifying road structures, water systems, etc.
\cite{isomorphismsearchinmassive}. For example, a researcher may want to count
the number of roundabouts in a particular region. This is an application of
subgraph isomorphism, but cartographers could use general subgraph search for
geographical structures with any properties, similar to the direct chemical
property search above.

Additionally, the problem of finding particular patterns of points and lines is
common in computer vision. Specifically, programmers often have to find
predefined patterns of landmarks within large images, containing countless
uncertain landmarks. With some creative cost functions, a solution to the
subgraph search problem can be used to find such features in images.

As evidenced by the previous applications, the optimal subgraph problem can be
applied to many more cases of pattern matching which we have not described here.
However, we believe that the applications we have described are exemplary of the
breadth of applicability of this problem.

\subsection{Challenges (JE, DJ, SS)}

There are several challenges associated with the optimal subgraph search problem
varying from implicit problem complexity to difficulty variation based on the
properties of the input. Perhaps the most important property of the optimal
subgraph search problem is that it is NP-complete. Due to the optimal subgraph
search problem's NP-completeness, it is currently impossible to develop an
algorithm that can find the optimal solution in polynomial time, so instead we
must resort to other means such as approximation algorithms. Following this,
another major challenge of the optimal subgraph search problem is its input
sensitivity. Major imbalances within the search graph, $L$, can make it very
difficult to effectively partition the instance of the problem to solve it in
parallel. This dependency on the topology of the graph is further exacerbated by
the sparsity or density of the large graph $L$. We anticipate that we will need
to utilize different approximation algorithms for sparse graphs and dense graphs
to most efficiently find good solutions.

\section{Tools and Approach (JE, DJ, SS)}

The languages we will use for building and evaluating our algorithms are Python,
Rust, and CUDA. Python will be used to create a single-threaded, baseline
implementation which we will use to evaluate the performance and accuracy of our
subsequent implementations. We opt to use Python as our baseline because it is a
commonly-used language that enables rapid prototyping of various algorithms to
quickly verify their correctness.

Once we have iterated on the Python baseline to find a satisfactory algorithm,
we will apply our first optimization: porting our program to Rust. We choose
Rust over C or C++ because Rust's performance is better or comparable to that of
C and C++ in most cases \cite{bmgame}.

Another notable benefit of Rust in a high performance computing context is its
ability to verify memory safety and thread safety at compile time. That is, it
is impossible to produce a (safe) Rust program that has data races, memory
leaks, or dangling pointers. Rust also provides cross paradigm abstractions
which have productivity advantages over pure object oriented (C++) and
procedural (C) programming \cite{rustlangorg}.

After a single threaded Rust implementation is complete, we will parallelize the
program to utilize multiple CPU cores, using the techniques discussed in class.
Finally, we plan to develop a CUDA C kernel to further accelerate the algorithm
on GPUs.

\subsection{Beam Search (SS)}

Although we will experiment with a large number of potential algorithms and
platforms, we plan to first use a beam search to implement our solution.

First, we must update the cost function to evaluate the \emph{potential} cost of
a subgraph. Specifically, the potential cost function will have to apply
usefully to incomplete matches. For example, a single vertex will minimize the
potential cost function if the vertex \emph{might} be a member of the best
possible subgraph. We will start by finding all such single vertices which pass
a given threshold. Then, we will iteratively consider larger and larger
subgraphs, with only graphs that pass the threshold being extended in the
iteration. Such an algorithm is not guaranteed to find the optimal subgraph, but
is expected to effectively minimize cost.

The algorithm might further be improved by a stochastic factor, for example by
applying a degree of simulated annealing or allowing some random cases to
temporarily pass the threshold.

\section{Evaluation Strategy (DJ)}

In order to evaluate the effectiveness of our implementations, we will use
Isengard, one of the available servers on campus. Isengard is a
24-physical-core, 48-thread machine composed of two Intel Xeon E5-2690 v3
processors running at 2.6 GHZ. Alongside its processor it has 378 GB of RAM. On
the GPU end it possess an Nvidia Titan GPU, but the exact specifications are
currently unknown due to driver issues on Isengard.

All of our implementations will be run on Isengard with inputs of varying size
and will be evaluated based on the runtime of the program (excluding input load
time) and total hardware utilization. Additionally, we will use standard
statistical methods to determine the accuracy of each of the implementations.

\section{Timeline (DJ)}

We will first complete our proof of concept implementation in Python by October
20th. This deliverable will include a Python script that is able to take in two
weighted graphs, $L$ and $P$, and perform a beam search on $L$ in order to find
the closest pattern to $P$. We will also create a program to randomly generate
random graphs with $n$ nodes so we can make large graphs to input into our
deliverable by this date. To verify the correctness of the implementation, we
will use a couple subgraph isomorphism cases and a couple of small subgraph
search cases.

Following the baseline implementation we will create an algorithmically
identical beam search implementation in Rust by November 2nd.  Once we have both
versions are implemented we will run several inputs on both programs in order to
verify the correctness of the Rust implementation, and to create a performance
comparison between the two implementations.

After we have verified the correctness of our Rust implementation, we will
utilize the following two weeks (up to November 16th) to focus on optimizing the
Rust algorithm to maximize performance. Some of the optimizations that we will
implement at this time will be parallelization of our implementation,
algorithmic optimization, and other optimization techniques discussed in the
class. Once the optimized version is produced we will compare it against the
original Rust implementation to measure the total performance improvement.

When the Rust-based implementation is complete, we will move onto attempting to
make an effective CUDA-based GPU implementation. If that is completed before
December 13th, we will compare the GPU implementation performance against the
performance of all of the CPU implementations.

\section{Conclusion (JE)}

In this proposal we have defined the optimal subgraph problem and shown its
general applicability to less specific problems including the subgraph
isomorphism problem. We discussed some real-world applications of the optimal
subgraph problem, and described some of the challenges we anticipate while
implementing this algorithm. We also laid out our planned approach and the tools
we will use to implement and optimize a solution to this problem. Lastly, we
described how we will evaluate our solution, and outlined a tentative timeline
for our project.

We believe that this project will be a difficult algorithmic challenge because
the problem is NP-complete, and finding a good heuristic will be non-trivial.
Additionally, and most importantly in the context of this course, we believe
that this problem will also be a difficult, and interesting, high performance
computing challenge because optimizing our solution to the optimal subgraph
search problem will require the use of many HPC techniques.

We look forward to your feedback on our proposal.

\section{Collaboration}

Each section is labeled with the initials of the person or people who did most
of the writing on that section. We primarily worked on the proposal in-person in
the ALAMODE lab, so there was significant cross-collaboration on all aspects of
the paper.

%% Bibliography
\bibliographystyle{unsrtnat}
\bibliography{../references}

%% Appendix
% \appendix
% \section{Appendix}

% Text of appendix \ldots

\end{document}
