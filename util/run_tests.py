#!/usr/bin/python3

from subprocess import call
import os
import time
import matplotlib.pyplot as plt
import numpy as np

WORLD_GRAPH_DIR = "world_inputs/"
PATTERN_GRAPH_DIR = "pattern_inputs/"
PYTHON_IMP = 'python-src/optimal_subgraph_search.py'
PYTHON_EVAL = 'python-src/evaluators/subgraph_isomorphism.py'
ITS = 10

DEVNULL = open(os.devnull, 'w')

times = {}
#for world_graph in os.listdir(WORLD_GRAPH_DIR):
world_graph = "5.txt"
for pattern_graph in os.listdir(PATTERN_GRAPH_DIR):
    t = []
    for _ in range(ITS):
        start_time = time.time_ns()
        call([PYTHON_IMP, WORLD_GRAPH_DIR + world_graph, PYTHON_EVAL, '--',
        PATTERN_GRAPH_DIR + pattern_graph], stdout=DEVNULL)
        elapsed_time = time.time_ns() - start_time
        t.append(elapsed_time)
    times["{}".format(pattern_graph)] = sum(t)/len(t)/1e9
        

    fout.write(str(times))

print(times)
